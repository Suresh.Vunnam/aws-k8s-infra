#!/bin/bash

hosted_zone_id=$1;
aws route53 list-resource-record-sets \
  --hosted-zone-id $hosted_zone_id |
jq -c '.ResourceRecordSets[]' |
while read -r resourcerecordset; do
  type=$(jq -r '.Type' <<<"$resourcerecordset")
  name=$(jq -r '.Name' <<<"$resourcerecordset")
  if [ $type == "NS" -o $type == "SOA" ]; then
    echo "SKIPPING: $type $name"
  else
    change_id=$(aws route53 change-resource-record-sets \
      --hosted-zone-id $hosted_zone_id \
      --change-batch '{"Changes":[{"Action":"DELETE","ResourceRecordSet":
          '"$resourcerecordset"'
        }]}' \
      --output text \
      --query 'ChangeInfo.Id')
    echo "DELETING: $type $name $change_id"
  fi
done
